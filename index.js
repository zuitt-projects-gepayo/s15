// console.log("hello world");

//Assignment Operators

//Basic Assign operator ( = )
    //it allows us to assign a value to a variable

    let variable = "initial Value";

//Mathematical Operators 

    //addition( + )
    //subtraction ( - )
    //Multiplication ( * )
    //Division ( / )
    //modulo ( % )

let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

//Additional Assignment Operator (+=)
//left operand is the variable or the value of the left side of the operator
//right operand ios athe variable or the value of the right side of the operator

//num1=num1+num4 (re-assigned the value of num1 with the result of num1+num4)
    num1 += num4;
    console.log(num1)//45

    num1+=55;
    console.log(num1)//100

    let string1="Boston";
    let string2="Celtics";

    string1+=string2;
    console.log(string1)//BostonCeltics

    // 15+=num1;
    // console.log(num1);//ERROR
    //NOTE: do not use assignment operator when the left operand is just data

//Subtraction Assignemnt Operator (-=)
    num1 -= num2;
    console.log(num1)//90

    num1 -= num4;
    console.log(num1)//50

    num1 -= 10;
    console.log(num1)//40

    num1 -= string1;
    console.log(num1)//NaN (Not a Number) -because staring1 is an alphanumeric

    string1-=string2;
    console.log(string1) //NaN (Not a Number)


//Multiplication Assignemnt Operator (*=)

    num2 *= num3;
    console.log(num2)//40

    num2 *= 5;
    console.log(num2)//200

//Division Assignemnt Operator (/=)

    num4/=num3;
    console.log(num4);//10

    num4/=2;
    console.log(num4);//5

// Mathematical Operations - we follow MDAS (Multiplication, Division, Addition, Subtraction)
    let mdasResult = 1+2-3*4/5;
    console.log(mdasResult);//0.6000000000000001

//PEMDAS - Parenthesis, Exponents, Multiplication, Division, Addition, Subtraction

    let pemdasResult = 1+(2-3)*(4/5);
    console.log(pemdasResult);//0.19999999999999996

//Increment and Decrement
    //2 kinds of Incrementation: Pre-fix and post-fix

    let z=1;
    //pre-fix incrementation;

    ++z;
    console.log(z);//2

    z++;
    console.log(z);//3

    console.log(++z);//4
    console.log(--z);//3
    console.log(z--)//3
    console.log(z)//2

//Comparision Operators
    //this is used to compare the values of the left and right operand
    //note that comparision operators returns a boolean

    //Equality or loose Equality operators (==)
    console.log(1==1);//true
    //we can also save the result of a comparison in a variable;

    let isSame = 55==55;
    console.log(isSame);

    console.log(1=="1");//true
    //loose quality prioritize the sameness of the value, force coercion is adone before comparision. forced coercion/force conversion -JS forcibly changing the data type of the operands.

    console.log(0 == false);//true
    //NOTE: false is converted into a number and  the equivalent of false is 0

    console.log(1 == true)//true
    //NOTE: true is equal to 1

    console.log("1"==1);//true

    console.log(true == "true");//false
    //since true=1 and boolean; not "true"


//Strict Equality Operator (===)
    console.log (true==="1");//false
    //NOTE: checks both the value and data type

    console.log("Johnny" == "Johnny");//true
    //NOTE: same datatype and same value


//Inquality Operations
    //loose Inequality (!=)

    console.log("1"!=1)//false

    console.log("Rose" != "Jennie")//true

    console.log(false != 0) //false
    //NOTE: false=0

    console.log(true!="true");//true

    //Strict Inequality (!==) 
        //it will check whether the two Operands have different values and will check if they have different data types
    
    console.log("5" !== 5);//true
    //NOTE:they have different data types

    console.log( 5 !== 5)//false
    //NOTE: same Value and same Data type

    console.log(true !=="true");//true
    


    let name1 = "Juan";
    let name2 = "Shane";
    let name3 = "Peter";
    let name4 = "jack";
    
    let number1 = 50;
    let number2 = 60;
    let numString1 = "50";
    let numString2 = "60";
    
    console.log(numString1 == number1);//true
    console.log(numString1 === number1)//false
    console.log(numString1 != number1);//false
    
    console.log(name4 !== name3);//true
    console.log(name1 == "jaun");//false
    console.log(name1 === "Juan");//true

//Relational Comparison Operators
    //A comparison operator which will check the relationship between operands

    let q = 500;
    let r = 700;
    let w = 8000;
    let numstring3 = "5500";

    //greater THan (>)
    console.log(q > r);//false
    console.log(w > r)//true

    //less than (<)
    console.log(w < q)//false
    console.log(q < 1000)//true

    console.log(numstring3 < 6000);//true
    //NOTE: Coercion effects here

    console.log(numstring3 < "Jose");// true
    //NOTE: this is erratic

    //greater THan (>=)

    console.log(w >= 8000);//true
    console.log (r>=q)//true

    //greater THan (<=)

    console.log(q<=r)// true
    console.log(w<=q)//false

//Logical Operators
    
    //And Operator (&&)
        //both operands on the left and the rights or all operands added must be true or will result to true
    
    let isAdmin = false;
    let isRegistered = true;
    let isLegalAge = true;

    let authorization1 = isAdmin && isRegistered;
    console.log(authorization1);// false

    let authorization2 = isRegistered && isLegalAge;
    console.log(authorization2);//true

    let requiredLevel = 95;
    let requiredAge = 18;
    let authorization3 = isRegistered && requiredLevel === 25;
    console.log(authorization3);

    let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
    console.log(authorization4);


    let userName1 = "gamer2001";
    let userName2 = "shadow1991";
    let userAge1 = 15;
    let userAge2 = 30;

    let registration1 = userName1.length > 8 && userAge1 >= requiredAge;
    console.log(registration1); // false

    let registration2 = userName2.length >8 && userAge2 >= requiredAge;
    console.log(registration2);


    //OR Operator (|| - double pipe)
        //Returns true if at least one the operands are true.

    let userLevel = 100;
    let userLevel2 = 65;


    let guildRequirement1 = isRegistered && userLevel >= requiredLevel && userAge1 >=requiredAge;
    console.log(guildRequirement1);// false


    let guildRequirement2 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
    console.log(guildRequirement2);//true

    let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
    console.log(guildAdmin);//false

    //Not Operator (!)
        //turns a boolean value into the opposite value

    console.log(!isRegistered);//false
    console.log(!guildAdmin);//true


// If-Else Statements
    // if statement will run a block of code if the condition specified is true or results to true

    // if(true){
    //     alert("we run an if condition")
    // };

    let userName3 = "crusader_1993";
    let userLevel3 = 25;
    let userAge3 = 20;

    if(userName3.length > 10){
        console.log("Welcome to the Game online!");
    };

    if (userLevel3 >= requiredLevel) {
        console.log("you are qualified to join the Guild")
    };

    if(userName3.length >= 10 && isRegistered && isAdmin){
        console.log("Thank you for Joining the Admin")
    };

    //else statement will run if the condition is false or results to false

    if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
        console.log("thank you for joining the noob Guild");
    }
    else {
        console.log("your to Strong to be noob");
    };

    //else if statements - executes a statement if the previous or the original condition is false or resulted to false  but another specified condition resulted to true

    if (userName3.length >=10 && userLevel3 <=25 && userAge3 >=requiredAge){
        console.log("thank you for joining the noob Guild");
    }
    else if (userLevel3 >25) {
        console.log("your to Strong to be noob");
    }
    else if (userAge3 < requiredAge) {
        console.log("You are too Youngto join the Guild")
    }
    else if (userName3.length<10) {
        console.log("Username is too Short")
    }
    else {
        console.log("Unable to Join");
    }


    function addNum (num1 , num2)
    {
        //typeof checks the data type
        //checks if the numbers being passed are number types
        if (typeof num1 === "number" && typeof num2 ==="number"){
            console.log("Run only if both arguments pass are tnumber types");
            console.log(num1+num2);
        }
        else {
            console.log("one or both of the arguments are not numbers");
        }
    }
    addNum(5,2);

    // function login (username, password) {
    //     if(typeof username === "string" && typeof password === "string"){
    //         console.log("both arguments are string")

    //         //nested if-else statement
    //         if(username.length >= 8 && password.length >=8) {
    //             alert("thank you for logging in")
    //         }
    //         else if (username.length <8) {
    //             alert("username is too short")
    //         }
    //         else if (password.length <8) {
    //             alert("password is too short")
    //         }
    //     }
    //     else {
    //         console.log("one of the arguments are not string type");
    //     }
    // }

    // login("theTinker","tinkerbell");


    //Switch Statements
        // is an alternative to an if, else if else tree, where the data being evaluated or checked is of an expected input.
    
    //Syntax:

        /*
            switch (expression / condition){
                case value:
                    statement;
                    break;
                default:
                    statement;
                    break;
            }

        */
    
    let hero = "Hercules";

    switch (hero) {
        case "Jose Rizal":
        console.log("National Hero of the Philippines");
        break;

        case "George Washinton":
        console.log("Hero of the American Revolution");
        break;

        case "Hercules":
        console.log("Legendary hero of the Greeks");
        break;

        default:
        console.log("none in the choices")
        break;
    }

    function roleChecker (role){
        switch (role) {
            case "Admin":
            console.log("Welcome Admin");
            break;
    
            case "User":
            console.log("Welcome User");
            break;
    
            case "Guest":
            console.log("Welcome Guest");
            break;
    
            default:
            console.log("Invalid Role")
            break;
        }
    }
    roleChecker("Admin");


    function gradeEvaluator (grade){
        if (grade >=90) {
            return "A";
        }
        else if (grade >=80) {
            return "B"; 
        }
        else if (grade >=71){
            return "C";
        }
        else if (grade <= 70){
            return "F";
        }
        else {
            return "Invalid Grade"
        }
    };
    let studentGrade = gradeEvaluator(85);
    console.log(studentGrade);

    //Ternary Operator
        // a shorthand way of writing if-else statements

            // SYNTAX:
                // condition ? if -statement : else -statement
    
    let price = 5000;

    price > 1000 ? console.log("Price is over 1000") : console.log("Price is less than 1000");

    let villian = "Harvy Dent";

    // villian ==="Harvey dent" ? console.log("You were supposed to be the chosen one ");
    // NOTE: else statement in ternary Operation is required

    villian === "Two Face" ? console.log("You live long enough to be a villian") : console.log("not quite villainous yet");

    